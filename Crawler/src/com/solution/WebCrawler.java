package com.solution;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class WebCrawler {
	
	//Default size of the pool 
	private static int Default_Pool_Size=10;
	
	//Default depth that should be taken 
	private static int Default_depth_level=10; 
	
	//Default number of documents to be processed 
	private static int Default_number_documents=500;
	
	public void fetchURL(String pageURL){
		
		this.onStart(pageURL, Default_Pool_Size, Default_depth_level,Default_number_documents);
		
	}
	public void fetchURL(String pageURL,int poolSize,int depthLevel,int numDocs){
		
		this.onStart(pageURL, poolSize, depthLevel,numDocs);
				
	}
	//Fetch the root URL and start the number of configured threads
	//that will fetch 
	private void onStart(String pageURL,int poolSize,int depthLevel,int numDocs){
		
		
		
		
	}
	private void onFinish(){
		
		
		
				
		
	}

}
