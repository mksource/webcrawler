package com.solution.service;

import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import com.solution.exceptions.NotFoundException;
import com.solution.util.ErrorMessages;

@Path("/rest/pin")
public class RestService {
	
	
	//This methods will generate a secondar_user_id for a given pin 
	//This will create 
	@PUT
	@Path("/update_secondary_user")
	public void updateSecondaryUser(@QueryParam("id") String pin,@QueryParam("secondary_user") String secondaryUser){
		
		
		//Step 1. Check if the pin exists if it does not then throw an error
		//First check in the cache    
		RestInvoker invoker=new RestInvoker();
		CacheService service=CacheService.getCacheService();
		String output=invoker.getPINInformation(pin);
		
		if(output == null){
			
			throw new NotFoundException(ErrorMessages.PIN_NOT_FOUND);
		}
		
		
		//Step 2. Check if secondary Id already exsists for this PIN
		
		
		
		//We make a db only when storing secondary ID other times we will t
		
		
		//Step 3. Make a connection to the db to store the id of the pin 
		
		
		
		//Step 4. cache the value for this PIN
		
		
		
		//Step 5. If successfully created then send 201 with Successfully created
		
		
		
		
		
	}
	
	
	
	@GET
	@Path("/find")
	public void getPIN(@Context UriInfo info){
		
			
		
		//if secondary_user_id is null then we need to fetch the PIN
		
		
		//If the pin is null then we need to fetch pin information based on the secondary user information
		
		
		
	}
	
	
	

}
