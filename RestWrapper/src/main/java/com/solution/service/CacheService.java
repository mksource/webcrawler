package com.solution.service;

import java.util.List;
import java.util.concurrent.ConcurrentNavigableMap;

public class CacheService {
	
	private  ConcurrentNavigableMap<String,String> pinToSecondaryMap;
	
	private  ConcurrentNavigableMap<String,List<String>> userToSecondary;
	
	private  ConcurrentNavigableMap<String,List<String>> secondaryTouser;
	
	private static CacheService cacheService;
	
			
	private CacheService(){
		
		
		
	}
	
	//Construct a Singleton for the CacheService
	public static CacheService getCacheService(){
		
		if(cacheService == null){
			
			synchronized(CacheService.class){
				
				if(cacheService == null){
					
					cacheService=new CacheService();
				}
				
			}
		}
			
			
		return cacheService;
		
	}
	
	//add secondaryId to a PIN 
	public String getSecondaryForPin(String pin){
		
		String secondaryId=null;
		if(pinToSecondaryMap.containsKey(pin))
			secondaryId=pinToSecondaryMap.get(pin);
		return secondaryId;
	}
	
	//add a userId to secondary (This has to be atomic and we acquire lock)
	
	
	
	
	
	
	//add a secondary user id for this user id

	
	

}
