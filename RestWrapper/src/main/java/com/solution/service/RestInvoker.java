package com.solution.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.solution.util.AppProperties;
import com.sun.jersey.core.util.Base64;



public class RestInvoker {
	
	
	public String getPINInformation(String pin){
		
		
		String resturl=AppProperties.PIN_URL;
		String output=null;
		HttpURLConnection conn=null;
		
		
		try{
			
			
			URL url=new URL(resturl+pin);
			conn=(HttpURLConnection)url.openConnection();
			
			//Set Basic Authrization 
			String userpass=AppProperties.USER_NAME+":"+AppProperties.USER_PASSWORD;
			String basicAuth = "Basic " + new String(new Base64().encode(userpass.getBytes()));
			conn.setRequestProperty ("Authorization", basicAuth);
			
			//Set the Request body
			conn.setRequestMethod("GET");
			
			//Set the Accept Type
			conn.setRequestProperty("Accept", "application/json");
			
				
			//Check for the response code
		    if(conn.getResponseCode() != 200){
		    	
		    	
		    		return output;
		    }
		    
		    
			
			//Read the output 
		    BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));
		 
		    String line=null;
		    StringBuffer result=new StringBuffer("");
			while ((line = br.readLine()) != null) {
					result.append(line);
			}	
			
			output=result.toString();
			System.out.println(output);
			
			//Check if the pin is not found
			if(doesConatinError(output))
				return null;
			
						
		}
		catch(MalformedURLException e){
			e.printStackTrace();
		}
		catch(IOException e){
			e.printStackTrace();
		}
		finally{
			//Disconnect the client
			if(conn!=null)
			conn.disconnect();
		}
		
		
		
		return output;
		
		
	}
	
	//Find if the json output contains error in the output
	public boolean doesConatinError(String output){
		
		
		
		JSONObject pin=(JSONObject) JSONValue.parse(output);
		
		if(pin.containsKey("error")){
			
			return true;
		}
		
		return false;
		
		
	}
	
	
	

}
